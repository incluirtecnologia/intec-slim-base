up:
	docker-compose up
down:
	docker-compose down
php:
	docker exec -it intec-base bash
install:
	composer install
update:
	composer update
log:
	grc -c conf.log tail -f var/logs/app.log