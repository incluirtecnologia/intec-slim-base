<?php

namespace Intec\IntecSlimBase\Handler;

use Intec\IntecSlimBase\Exception\Domain\GenericDomainException;
use Slim\Exception\HttpNotFoundException;

class AppErrorHandler extends ErrorHandler
{
    protected $displayErrorDetails;
    protected $logErrors;
    protected $logErrorDetails;
    protected $contentType;
    protected $method;
    protected $request;
    protected $exception;
    protected $statusCode;
    protected $callableResolver;
    protected $responseFactory;
    protected $logger;

    private array $ignoreExceptions = [
        HttpNotFoundException::class,
        GenericDomainException::class,
    ];

    protected function logError(string $error): void
    {
        $exceptionClass = get_class($this->exception);

        if (in_array($exceptionClass, $this->ignoreExceptions)) {
            return;
        }

        parent::logError($error);
    }

    protected function determineStatusCode(): int
    {
        if ($this->exception instanceof GenericDomainException) {
            return 400;
        }

        return parent::determineStatusCode();
    }
}
