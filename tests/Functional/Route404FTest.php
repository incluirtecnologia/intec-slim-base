<?php

namespace Intec\IntecSlimBase\Test\Functional;

class Route404FTest extends TestCase
{
    public function testInvalidRouteWillReturnHttpStatusCode404()
    {
        $request = $this->createRequest('GET', '/invalid-route');
        $response = $this->app->handle($request);

        $this->assertEquals(404, $response->getStatusCode());
        $this->assertEquals('Not Found', $response->getReasonPhrase());
        $this->assertEquals(['application/json'], $response->getHeader('content-type'));
    }
}
