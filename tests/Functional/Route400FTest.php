<?php

namespace Intec\IntecSlimBase\Test\Functional;

class Route400FTest extends TestCase
{
    public function testRouteError400WillReturnHttpStatusCode400()
    {
        $this
            ->logger
            ->expects($this->once())
            ->method('error')
            ->with(
                $this->stringContains(self::HTTP_400_ERROR_MESSAGE)
            );

        $request = $this->createRequest('POST', '/route-error-400');
        $response = $this->app->handle($request);

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertEquals('Bad Request', $response->getReasonPhrase());
        $this->assertEquals(['application/json'], $response->getHeader('content-type'));
    }
}
