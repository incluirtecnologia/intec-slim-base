<?php

namespace Intec\IntecSlimBase\Test\Unit\Helper;

use Intec\IntecSlimBase\Helper\JsonResponse;
use PHPUnit\Framework\TestCase;
use Slim\Psr7\Response;

class JsonResponseTest extends TestCase
{
    use JsonResponse;

    public function testReturnsCorrectDefaultResponse()
    {
        $resp = new Response();
        $responseData = ['some' => 'data'];
        $expectedResponseData = $this->getResponseDataArray(200, 'ok', $responseData);

        $jsonResponse = $this->toJson(response: $resp, data: $responseData);
        $actualResponseData = json_decode($jsonResponse->getBody(), true);

        $this->assertEquals(200, $jsonResponse->getStatusCode());
        $this->assertEquals('ok', $jsonResponse->getReasonPhrase());
        $this->assertEquals($expectedResponseData, $actualResponseData);
    }

    public function testReturnsCorrectResponse()
    {
        $code = 201;
        $message = 'created';
        $responseData = ['age' => 26, 'birth' => '1991-12-06'];
        $expectedResponseData = $this->getResponseDataArray($code, $message, $responseData);

        $resp = new Response();

        $jsonResponse = $this->toJson($resp, $code, $message, $responseData);
        $actualResponseData = json_decode($jsonResponse->getBody(), true);

        $this->assertEquals($jsonResponse->getStatusCode(), $code);
        $this->assertEquals('created', $jsonResponse->getReasonPhrase());
        $this->assertEquals(['application/json'], $jsonResponse->getHeader('content-type'));
        $this->assertEquals($expectedResponseData, $actualResponseData);
    }

    private function getResponseDataArray(int $code, string $message, array $data)
    {
        return [
            'code' => $code,
            'message' => $message,
            'data' => $data
        ];
    }
}
