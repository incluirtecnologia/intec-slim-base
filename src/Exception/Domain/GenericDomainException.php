<?php

namespace Intec\IntecSlimBase\Exception\Domain;

use Exception;
use InvalidArgumentException;
use Throwable;

class GenericDomainException extends Exception
{
    const FIRST_VALID_CODE = 100_000_000;

    public function __construct(
        protected array $domainInfo = [],
        protected $message = 'generic domain error',
        protected $code = self::FIRST_VALID_CODE,
        protected ?Throwable $previous = null
    ) {
        if ($this->code < self::FIRST_VALID_CODE) {
            throw new InvalidArgumentException("Invalid domain exception code '{$this->code}'.");
        }
    }
}
