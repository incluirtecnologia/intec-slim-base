<?php

namespace Intec\IntecSlimBase\Action;

use Intec\IntecSlimBase\Helper\JsonResponse;

abstract class JsonAction
{
    use JsonResponse;
}
