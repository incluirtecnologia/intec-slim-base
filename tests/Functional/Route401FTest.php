<?php

namespace Intec\IntecSlimBase\Test\Functional;

class Route401FTest extends TestCase
{
    public function testRouteError401WillReturnHttpStatusCode401()
    {
        $this
            ->logger
            ->expects($this->once())
            ->method('error')
            ->with(
                $this->stringContains(self::HTTP_401_ERROR_MESSAGE)
            );

        $request = $this->createRequest('POST', '/route-error-401');
        $response = $this->app->handle($request);

        $this->assertEquals(401, $response->getStatusCode());
        $this->assertEquals('Unauthorized', $response->getReasonPhrase());
        $this->assertEquals(['application/json'], $response->getHeader('content-type'));
    }
}
