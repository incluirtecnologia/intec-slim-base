<?php

namespace Intec\IntecSlimBase\Handler;

class AppErrorIgnoreHandler extends ErrorHandler
{
    protected $displayErrorDetails;
    protected $logErrors;
    protected $logErrorDetails;
    protected $contentType;
    protected $method;
    protected $request;
    protected $exception;
    protected $statusCode;
    protected $callableResolver;
    protected $responseFactory;
    protected $logger;

    protected function logError(string $error): void
    {
        parent::logError($error);
    }

    protected function determineStatusCode(): int
    {
        $statusCode = parent::determineStatusCode();

        return $statusCode < 400 ? $statusCode : 200;
    }
}