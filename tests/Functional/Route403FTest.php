<?php

namespace Intec\IntecSlimBase\Test\Functional;

class Route403FTest extends TestCase
{
    public function testRouteError403WillReturnHttpStatusCode403()
    {
        $this
            ->logger
            ->expects($this->once())
            ->method('error')
            ->with(
                $this->stringContains(self::HTTP_403_ERROR_MESSAGE)
            );

        $request = $this->createRequest('POST', '/route-error-403');
        $response = $this->app->handle($request);

        $this->assertEquals(403, $response->getStatusCode());
        $this->assertEquals('Forbidden', $response->getReasonPhrase());
        $this->assertEquals(['application/json'], $response->getHeader('content-type'));
    }
}
