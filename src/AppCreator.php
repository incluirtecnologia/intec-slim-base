<?php

namespace Intec\IntecSlimBase;

use Closure;
use Exception;
use Intec\IntecSlimBase\Handler\AppErrorHandler;
use Intec\IntecSlimBase\Handler\ErrorHandler;
use Intec\IntecSlimBase\Renderer\JsonErrorRenderer;
use Psr\Log\LoggerInterface;
use Slim\App;

class AppCreator
{
    public static function createApp(
        array $settings,
        array $dependencies,
        Closure $routes,
        string $errorHandler = AppErrorHandler::class,
        string $errorRenderer = JsonErrorRenderer::class
    ): App {
        $builder = new \DI\ContainerBuilder();
        $builder->useAutowiring(true);
        $builder->useAnnotations(false);

        if ($settings['app.cache_enabled']) {
            $builder->enableCompilation(__DIR__ . '/../var/cache');
            $builder->writeProxiesToFile(true, __DIR__ . '/../var/cache/proxies');
        }

        $builder->addDefinitions($settings);
        $builder->addDefinitions($dependencies);

        $container = $builder->build();

        $app = \DI\Bridge\Slim\Bridge::create($container);

        $routes($app);

        $app->addRoutingMiddleware();

        $callableResolver = $app->getCallableResolver();
        $responseFactory = $app->getResponseFactory();
        $logger = $container->get(LoggerInterface::class);

        if(!is_subclass_of($errorHandler, ErrorHandler::class)) {
            throw new Exception("$errorHandler must be of type " . ErrorHandler::class);
        }

        /** @psalm-suppress UnsafeInstantiation **/
        $errorHandler = new $errorHandler($callableResolver, $responseFactory, $logger);
        $errorHandler->registerErrorRenderer('application/json', $errorRenderer);
        $errorHandler->forceContentType('application/json');

        $errorMiddleware = $app->addErrorMiddleware(
            (bool) $settings['app.display_error_details'],
            true,
            true,
            $container->get(LoggerInterface::class)
        );
        $errorMiddleware->setDefaultErrorHandler($errorHandler);

        $routeCollector = $app->getRouteCollector();
        if ($settings['app.cache_enabled']) {
            $routeCollector->setCacheFile(__DIR__ . '/../var/cache/routes.cache.php');
        }

        return $app;
    }
}
