<?php

namespace Intec\IntecSlimBase\Exception\Http;

use Slim\Exception\HttpSpecializedException;

class HttpUnprocessableEntityException extends HttpSpecializedException
{
    protected $request;
    protected $message = 'Unprocessable Entity';
    protected $code = 422;
    protected $title = '422 Unprocessable Entity';
    protected $description = 'The client should not repeat this request without modification.';
}
