<?php

namespace Intec\IntecSlimBase\Renderer;

use Psr\Http\Message\ResponseInterface;

class JsonResponseRenderer
{
    public static function toJson(
        ResponseInterface $response,
        array $data = [],
        string $reasonPhrase = '',
        int $httpCode = 200
    ): ResponseInterface {
        $json = json_encode($data, JSON_THROW_ON_ERROR);
        $response->getBody()->write($json);

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus($httpCode, $reasonPhrase);
    }
}
