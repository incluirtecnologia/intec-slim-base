<?php

namespace Intec\IntecSlimBase\Test\Functional;

class Route200FTest extends TestCase
{
    public function testRouteSlashHealthzWillReturnHttpStatusCode200(): void
    {
        $request = $this->createRequest('GET', '/healthz');
        $response = $this->app->handle($request);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('ok', $response->getReasonPhrase());
        $this->assertEquals(['application/json'], $response->getHeader('content-type'));
    }
}
