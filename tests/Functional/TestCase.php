<?php

namespace Intec\IntecSlimBase\Test\Functional;

use Closure;
use Error;
use Exception;
use Intec\IntecSlimBase\AppCreator;
use Intec\IntecSlimBase\Handler\AppErrorHandler;
use Intec\IntecSlimBase\Renderer\JsonErrorRenderer;
use Intec\IntecSlimBase\Renderer\JsonResponseRenderer;
use Intec\IntecSlimBase\Test\Helper\DummyValidation;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;
use PHPUnit\Framework\TestCase as FrameworkTestCase;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;
use Slim\App;
use Slim\Exception\HttpBadRequestException;
use Slim\Psr7\Factory\StreamFactory;
use Slim\Psr7\Headers;
use Slim\Psr7\Request;
use Slim\Psr7\Uri;
use PHPUnit\Framework\MockObject\MockObject;
use Slim\Exception\HttpForbiddenException;
use Slim\Exception\HttpUnauthorizedException;

class TestCase extends FrameworkTestCase
{
    const HTTP_400_ERROR_MESSAGE = 'Woww! some message for http bad request exception!';
    const HTTP_401_ERROR_MESSAGE = 'Woww! Invalid token!';
    const HTTP_403_ERROR_MESSAGE = 'Woww! Forbidden!';
    const HTTP_500_ERROR_MESSAGE = 'Woww! really bad error!';

    protected array $settings;
    protected array $dependencies;
    protected Closure $routes;
    protected string $errorHandler;
    protected string $errorRenderer;
    protected Logger|MockObject $logger;
    protected App $app;

    public function setUp(): void
    {
        $this->createAppInstance();
    }

    private function createAppInstance()
    {
        $this->settings ??= $this->loadDefaultSettings();
        $this->logger ??= $this->createMock(Logger::class);
        $this->dependencies ??= $this->loadDefaultDependencies();
        $this->routes = $this->loadDefaultRoutes();
        $this->errorHandler = AppErrorHandler::class;
        $this->errorRenderer = JsonErrorRenderer::class;

        $this->app = AppCreator::createApp(
            $this->settings,
            $this->dependencies,
            $this->routes,
            $this->errorHandler,
            $this->errorRenderer
        );
    }

    protected function createRequest(
        string $method,
        string $path,
        array $parsedBody = [],
        array $headers = ['HTTP_ACCEPT' => 'application/json'],
        array $serverParams = [],
        array $cookies = []
    ): ServerRequestInterface {
        $uri = new Uri('', '', 80, $path);
        $handle = fopen('php://temp', 'w+');
        $stream = (new StreamFactory())->createStreamFromResource($handle);

        $h = new Headers();
        foreach ($headers as $name => $value) {
            $h->addHeader($name, $value);
        }

        $request = new Request($method, $uri, $h, $cookies, $serverParams, $stream);

        return $request->withParsedBody($parsedBody);
    }

    private function loadDefaultSettings(): array
    {
        return [
            'app.cache_enabled' => false,
            'app.name' => 'MyTestApplication',
            'app.display_error_details' => false,
            'logger.name' => 'logger_name',
            'logger.path' => __DIR__ . '/../../var/logs/app.log',
            'logger.level' => Logger::DEBUG,
        ];
    }

    private function loadDefaultDependencies(): array
    {
        return [
            LoggerInterface::class => (fn () => $this->logger),
        ];
    }

    public function loadDefaultRoutes(): Closure
    {
        return function (App $app) {
            $app->get('/healthz', function (ResponseInterface $response) {

                return JsonResponseRenderer::toJson(
                    response: $response,
                    reasonPhrase: 'ok',
                    httpCode: 200
                );
            });

            $app->post('/route-error-400', function (ServerRequestInterface $request) {
                throw new HttpBadRequestException($request, TestCase::HTTP_400_ERROR_MESSAGE);
            });

            $app->post('/route-error-401', function (ServerRequestInterface $request) {
                throw new HttpUnauthorizedException($request, TestCase::HTTP_401_ERROR_MESSAGE);
            });

            $app->post('/route-error-403', function (ServerRequestInterface $request) {
                throw new HttpForbiddenException($request, TestCase::HTTP_403_ERROR_MESSAGE);
            });

            $app->post('/route-error-500', function (ServerRequestInterface $request) {
                throw new Error(TestCase::HTTP_500_ERROR_MESSAGE);
            });

            $app->post('/route-error-500-from-exception', function (ServerRequestInterface $request) {
                throw new Exception(TestCase::HTTP_500_ERROR_MESSAGE);
            });

            $app->post('/route-error-422', function (ResponseInterface $response) {

                return JsonResponseRenderer::toJson(
                    response: $response,
                    reasonPhrase: 'ok',
                    httpCode: 200
                );
            })->add(DummyValidation::class);
        };
    }

    protected function createRealLogger()
    {
        $loggerPath = $this->settings['logger.path'];
        $loggerLevel = $this->settings['logger.level'];
        $logger = new Logger($this->settings['logger.name']);

        $processor = new UidProcessor();
        $logger->pushProcessor($processor);

        $handler = new StreamHandler($loggerPath, $loggerLevel);
        $logger->pushHandler($handler);

        return $logger;
    }

    public function tearDown(): void
    {
    }
}
