<?php

namespace Intec\IntecSlimBase\Test\Functional;

class Route500FTest extends TestCase
{
    public function testRouteError500WillReturnHttpStatusCode500()
    {
        $this
            ->logger
            ->expects($this->once())
            ->method('error')
            ->with(
                $this->stringContains(self::HTTP_500_ERROR_MESSAGE)
            );

        $request = $this->createRequest('POST', '/route-error-500');
        $response = $this->app->handle($request);

        $this->assertEquals(500, $response->getStatusCode());
        $this->assertEquals('Internal Server Error', $response->getReasonPhrase());
        $this->assertEquals(['application/json'], $response->getHeader('content-type'));
    }

    public function testRouteError500FromExceptionWillReturnHttpStatusCode500()
    {
        $this
            ->logger
            ->expects($this->once())
            ->method('error')
            ->with(
                $this->stringContains(self::HTTP_500_ERROR_MESSAGE)
            );

        $request = $this->createRequest('POST', '/route-error-500-from-exception');
        $response = $this->app->handle($request);

        $this->assertEquals(500, $response->getStatusCode());
        $this->assertEquals('Internal Server Error', $response->getReasonPhrase());
        $this->assertEquals(['application/json'], $response->getHeader('content-type'));
    }
}
