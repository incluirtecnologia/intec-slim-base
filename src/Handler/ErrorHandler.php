<?php

namespace Intec\IntecSlimBase\Handler;

use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Log\LoggerInterface;
use Slim\Handlers\ErrorHandler as SlimErrorHandler;
use Slim\Interfaces\CallableResolverInterface;

abstract class ErrorHandler extends SlimErrorHandler
{
    protected $displayErrorDetails;
    protected $logErrors;
    protected $logErrorDetails;
    protected $contentType;
    protected $method;
    protected $request;
    protected $exception;
    protected $statusCode;

    /**
     * @param CallableResolverInterface $callableResolver
     * @param ResponseFactoryInterface  $responseFactory
     * @param LoggerInterface|null      $logger
     */
    final public function __construct(
        CallableResolverInterface $callableResolver,
        ResponseFactoryInterface $responseFactory,
        ?LoggerInterface $logger = null
    ) {
        $this->callableResolver = $callableResolver;
        $this->responseFactory = $responseFactory;
        $this->logger = $logger ?: $this->getDefaultLogger();
    }
}
