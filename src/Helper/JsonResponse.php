<?php

namespace Intec\IntecSlimBase\Helper;

use Intec\IntecSlimBase\Renderer\JsonResponseRenderer;
use Psr\Http\Message\ResponseInterface;

trait JsonResponse
{
    protected function toJson(
        ResponseInterface $response,
        int $statusCode = 200,
        string $message = 'ok',
        array $data = []
    ): ResponseInterface {
        $data = [
            'code' => $statusCode,
            'message' => $message,
            'data' => $data
        ];

        return JsonResponseRenderer::toJson($response, $data, $message, $statusCode);
    }
}
